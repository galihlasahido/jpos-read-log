package com.ads.jpos.readlog.background;


import com.google.gson.Gson;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOUtil;
import org.jpos.q2.Q2;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.context.annotation.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * com.ads.jpos.readlog.background
 * Created by galih.lasahido@gmail.com on 8/27/17.
 * read-log
 */
@Configuration
public class LoggerReader extends Q2 {
    public static String xmlToJson(String xml){
        String json ="";
        try {
            JSONObject xmlJSONObj = XML.toJSONObject(xml);
            json = xmlJSONObj.toString();
        } catch (JSONException je) {
            System.out.println(je.toString());
        }
        return json;
    }


    @PostConstruct
    void init() throws ISOException, IOException {
        LoggerReader q2 = new LoggerReader();
        q2.start();
        try {
            File file = new File("/Users/bankdki/Code/experimental/read-log/kartuku-26-09-2016-18.log");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            Map<String, String> map = new TreeMap<>();
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("log");

            for (int count = 0; count < nList.getLength(); count++) {
                Node nnList = nList.item(count);

                System.out.println("node name :" + nnList.getNodeName());
                System.out.println("node value :" + nnList.getNodeValue());

                NodeList childNodes = nnList.getChildNodes();

                Node childNodesitem = childNodes.item(1);
                switch (childNodesitem.getNodeName()) {
                    case "receive":
                        NodeList receivechildNodesitem = childNodesitem.getChildNodes();
                        Node nodereceivechildeNodeItem = receivechildNodesitem.item(1);
                        NodeList nl = nodereceivechildeNodeItem.getChildNodes();
                        for (int nlcount = 1; nlcount < nl.getLength(); nlcount++) {
                            Node nlnode = nl.item(nlcount);

                            if (nlnode.hasAttributes()) {
                                // get attributes names and values
                                NamedNodeMap nodeMap = nlnode.getAttributes();
                                Node node = nodeMap.item(0);
                                Node node2 = nodeMap.item(1);
                                if(node.getNodeValue().equalsIgnoreCase("61")) {
                                    if(node2.getNodeValue().length()==48) {
                                        map.put(node.getNodeValue(), hexToAscii(node2.getNodeValue()));
                                    } else {
                                        map.put(node.getNodeValue(), node2.getNodeValue());
                                    }
                                } else {
                                    map.put(node.getNodeValue(), node2.getNodeValue());
                                }
                            }
                        }

                        Gson gson = new Gson();
                        String toJson = gson.toJson(map);
                        System.out.println("toJson "+toJson);
                        System.out.println("[close] ");
                        break;

                    case "send":
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static String hexToAscii(final String hex) {
        if (hex == null) {
            return "";
        }
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hex.length(); i += 2) {
            String str = hex.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }
        return output.toString();
    }
}
