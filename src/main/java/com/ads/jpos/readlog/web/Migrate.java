package com.ads.jpos.readlog.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

/**
 * com.ads.jpos.readlog.web
 * Created by galih.lasahido@gmail.com on 8/28/17.
 * read-log
 */
@RestController
public class Migrate {

    @GetMapping("/migrate")
    @ResponseBody
    public String migrate() {
        File file = new File("C:/MyFolder/");
        File[] files = file.listFiles();
        for(File f: files){
            System.out.println(f.getName());
        }

        return "";
    }
}
